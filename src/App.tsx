import * as React from 'react';
import UsersList from './components/UsersList';

const App: React.FunctionComponent<unknown> = () => {
  return (
    <main className="container max-w-screen-lg m-auto shadow-sm py-2">
      <UsersList />
    </main>
  );
};

export default App;
