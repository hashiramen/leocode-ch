import * as React from 'react';
import Row from './Row';

export interface IDataTableProps {
  className?: string;
  data: any[];
  columns: {
    field: string;
    searchable?: boolean;
  }[];
  options?: {
    keyField?: string;
    search: boolean;
  };
  components?: {
    Row: React.FunctionComponent<{
      index: number;
      rowData: any;
      columns: IDataTableProps['columns'];
    }>;
  };
}

const DataTable: React.FunctionComponent<IDataTableProps> = ({
  className,
  data = [],
  columns = [],
  options = {
    keyField: undefined,
    rowsPerPage: 40,
    search: false,
  },
  components,
}) => {
  const [rows, setRows] = React.useState(data);

  React.useEffect(() => {
    setRows(data);
  }, [data]);

  const filterByValue = React.useCallback(
    (value, rowData) =>
      Object.keys(rowData).some((propKey) => {
        const target = columns.find((col) => col.field === propKey);

        const strictlySearchable = !(
          target &&
          typeof target.searchable === 'boolean' &&
          !target.searchable
        );

        const canCompare = target || !columns.length;

        return strictlySearchable
          ? canCompare
            ? rowData[propKey]
                .toString()
                .toLowerCase()
                .search(value?.toLowerCase()) > -1
            : false
          : false;
      }),
    [columns]
  );

  const handleSearch = React.useCallback(
    (event) => {
      const text = event.target.value;

      const newState = !text
        ? [...data]
        : [...data.filter((row) => filterByValue(text, row))];

      setRows(newState);
    },
    [data, filterByValue]
  );

  const RowNode =
    typeof components?.Row === 'function' ? components.Row : Row;

  const getKeyField = React.useCallback(
    (rowData, index) =>
      typeof options.keyField === 'undefined'
        ? index
        : rowData[options.keyField],
    [options]
  );

  return (
    <div className={className}>
      {options.search && (
        <input
          type="text"
          name="searchTerm"
          onChange={handleSearch}
          placeholder="Search by user name..."
          className="p-2 w-full font-semibold"
          autoFocus
        />
      )}
      <table className="mt-2">
        <tbody>
          {rows.map((rowData, index) => (
            <RowNode
              index={index}
              key={getKeyField(rowData, index)}
              rowData={rowData}
              columns={columns}
            />
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default DataTable;
