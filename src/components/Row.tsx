import * as React from 'react';
import { IDataTableProps } from './DataTable';

interface IRowProps {
  index: number;
  rowData: any;
  columns: IDataTableProps['columns'];
}

const Row: React.FunctionComponent<IRowProps> = React.memo(
  ({ rowData = {}, columns = [] }) => {
    return (
      <tr>
        {columns.map(({ field }, index) => (
          <td key={index}>{rowData[field]}</td>
        ))}
      </tr>
    );
  }
);

export default Row;
