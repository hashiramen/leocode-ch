import * as React from 'react';
import { IUserDTO } from './commons';
import { IDataTableProps } from './DataTable';

interface IUserItemProps {
  index: number;
  rowData: IUserDTO;
  columns: IDataTableProps['columns'];
}

const UserItem: React.FunctionComponent<IUserItemProps> = ({
  rowData,
  index,
}) => {
  return (
    <tr>
      <td className="space-x-2">
        <span className="text-gray-400">{index}.</span>
        <span className="text-black font-semibold">
          {rowData.name}
        </span>
        <span className="text-gray-400">@{rowData.username}</span>
      </td>
    </tr>
  );
};

export default UserItem;
