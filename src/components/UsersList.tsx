import * as React from 'react';
import { IUserDTO } from './commons';
import DataTable from './DataTable';
import UserItem from './UserItem';

interface IUsersListProps {}

const UsersList: React.FunctionComponent<IUsersListProps> = (
  props
) => {
  const [users, setUsers] = React.useState<IUserDTO[]>([]);

  React.useEffect(() => {
    const getUsers = async () => {
      const response = await fetch(
        'https://jsonplaceholder.typicode.com/users'
      );

      if (response.ok) {
        const users: IUserDTO[] = await response.json();
        setUsers(users);
      }
    };

    getUsers();
  }, []);

  return (
    <React.Fragment>
      <section className="flex justify-center h-full p-2 w-full bg-black rounded-t">
        <h1 className="font-semibold text-xl uppercase text-white tracking-wide">
          Users list
        </h1>
      </section>
      <section className="flex h-full p-2 w-full bg-gray rounded-b">
        <DataTable
          className="w-full"
          data={users}
          options={{
            search: true,
          }}
          columns={[
            {
              field: 'name' as keyof IUserDTO,
            },
            {
              field: 'username' as keyof IUserDTO,
              searchable: false,
            },
          ]}
          components={{
            Row: (props) => <UserItem {...props} />,
          }}
        />
      </section>
    </React.Fragment>
  );
};

export default UsersList;
