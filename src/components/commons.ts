export interface IUserDTO {
  id: number;
  name: string;
  username: string;
}
